import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  State<AuthPage> createState() => _AuthPage();
}

class _AuthPage extends State<AuthPage> {
  bool _signIn = true;
  String _email = '';
  String _password = '';

  final _formKey = GlobalKey<FormState>();

  Future<void> signIn() async {
    try {
      var userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _password);
      print(userCredential);
    } on FirebaseAuthException catch (ex) {
      var error = "";
      if (ex.code == "user-not-found") {
        error = "User is not found";
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(error)),
      );
    } catch (ex) {
      print(ex);
    }
  }

  Future<void> signUp() async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: _email, password: _password);
    } on FirebaseAuthException catch (e) {
      var error = "";
      if (e.code == 'weak-password') {
        error = ('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        error = ('The account already exists for that email.');
      }

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(error)),
      );
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(_signIn ? "Sign in " : "Sign up",
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 30)),
            ],
          ),
          Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(labelText: "Email"),
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (value) {
                      setState(() {
                        _email = value;
                      });
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter an email';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: const InputDecoration(labelText: "Password"),
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    onChanged: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter a password';
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      children: [
                        ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _signIn ? signIn() : signUp();
                            }
                          },
                          child: const Text('Submit'),
                        ),
                        MaterialButton(
                            onPressed: () {
                              setState(() {
                                _signIn = !_signIn;
                              });
                            },
                            child:
                                Text(_signIn ? 'Create an account' : 'Sign in'))
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
