import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'auth.dart';
import 'mcs.dart';
import 'prescriptions.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState() {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      setState(() {
        signedIn = user != null;
      });
    });
  }

  int _navIndex = 0;
  bool signedIn = false;

  final List<Widget> _pages = <Widget>[
    const PrescriptionsPage(),
    const McsPage()
  ];

  @override
  Widget build(BuildContext context) {
    if (!signedIn) {
      return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: const AuthPage());
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            IconButton(
              icon: const Icon(Icons.chevron_right),
              color: Colors.white,
              onPressed: () async {
                await FirebaseAuth.instance.signOut();
              },
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _navIndex,
          onTap: (idx) => {
            setState(() {
              _navIndex = idx;
            })
          },
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.local_hospital), label: "Prescriptions"),
            BottomNavigationBarItem(
                icon: Icon(Icons.document_scanner), label: "MCs")
          ],
        ),
        body: _pages.elementAt(_navIndex));
  }
}
