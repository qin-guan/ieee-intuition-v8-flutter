import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class McsPage extends StatelessWidget {
  const McsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        const Padding(
            padding: EdgeInsets.all(8.0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text("Medical Certificates",
                    style:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.bold)))),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Tai Seng Clinic",
                          style: TextStyle(fontSize: 20)),
                      Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(text: "Valid from: 11-10-2022\n"),
                            TextSpan(text: "Valid from: 11-15-2022\n"),
                          ]))),
                    ]),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Bishan Clinic",
                          style: TextStyle(fontSize: 20)),
                      Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(text: "Valid from: 11-10-2022\n"),
                            TextSpan(text: "Valid from: 11-15-2022\n"),
                            TextSpan(
                                text: "Shared with: ACME Company Pte Ltd\n"),
                          ]))),
                    ]),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Clementic Clinic",
                          style: TextStyle(fontSize: 20)),
                      Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: RichText(
                              text: TextSpan(children: [
                            TextSpan(text: "Valid from: 11-10-2022\n"),
                            TextSpan(text: "Valid from: 11-15-2022\n"),
                          ]))),
                    ]),
              ),
            ),
          ),
        ),
      ],
    ));
  }
}
