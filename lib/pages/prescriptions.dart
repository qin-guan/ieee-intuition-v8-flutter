import 'package:flutter/material.dart';

class PrescriptionsPage extends StatelessWidget {
  const PrescriptionsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // CollectionReference prescriptions =
    //     FirebaseFirestore.instance.collection('prescription');
    //
    // Future<void> addPrescription() {
    //   return prescriptions
    //       .add({"user": "hello"})
    //       .then((v) => print(v))
    //       .catchError((e) => print(e));
    // }

    return Scaffold(
        // floatingActionButton: FloatingActionButton(
        //   child: const Icon(Icons.add),
        //   onPressed: addPrescription,
        // ),
        body: Column(
      children: [
        const Padding(
            padding: EdgeInsets.all(8.0),
            child: Align(
                alignment: Alignment.topLeft,
                child: Text("Prescriptions",
                    style:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.bold)))),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Panadol", style: TextStyle(fontSize: 20)),
                      const Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("Twice everyday, after food"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: "Progress ",
                              ),
                              TextSpan(
                                  text: "5/10 days",
                                  style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                        ),
                      )
                    ]),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Panadol", style: TextStyle(fontSize: 20)),
                      const Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("Twice everyday, after food"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: "Progress ",
                              ),
                              TextSpan(
                                  text: "5/10 days",
                                  style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                        ),
                      )
                    ]),
              ),
            ),
          ),
        ),
        SizedBox(
          width: double.infinity,
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Panadol", style: TextStyle(fontSize: 20)),
                      const Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("Twice everyday, after food"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                text: "Progress ",
                              ),
                              TextSpan(
                                  text: "5/10 days",
                                  style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                        ),
                      )
                    ]),
              ),
            ),
          ),
        )
      ],
    ));
  }
}
